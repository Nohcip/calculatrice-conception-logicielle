import uvicorn
from fastapi import FastAPI
from pydantic import BaseModel


app = FastAPI()

#request body
class Nombres(BaseModel):
    num1: int
    num2: int


@app.post("/ajouter")
def somme(nombres: Nombres):
    return {"resultat": int(nombres.dict()["num1"])+int(nombres.dict()["num2"]) }

@app.post("/soustraire")
def soustraction(nombres: Nombres):
    return {"resultat": int(nombres.dict()["num1"])-int(nombres.dict()["num2"]) }

@app.post("/multiplier")
def mult(nombres: Nombres):
    return {"resultat": int(nombres.dict()["num1"])*int(nombres.dict()["num2"]) }

@app.post("/diviser")
def div(nombres: Nombres):
    # Gestion de la division par zero
    if nombres.dict()["num2"] != 0:
    # resultat arrondi à 0.01 près
        res = round(int(nombres.dict()["num1"])/int(nombres.dict()["num2"]), 2)
    else:
        res = "Division par zero impossible."
    return {"resultat": res }




