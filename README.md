# Conception Logicielle - Calculatrice 

# Calculatrice 

auteur : Guillaume Pichon 

L'objectif de ce projet Python est de concevoir un web service d'une calculatrice permettant de réaliser des opérations algébriques basiques.
Un client fait appel à ce webservice pour réaliser ces opérations. 
Enfin, un scénario de calcul d'une épreuve du jeu "Des Chiffres et des Lettres" est implémenté.

## Installation 

Pour installer les dépendances nécessaires au projet, saisir dans un terminal : 
pip install -r requirements.txt

## Quickstart 

Pour lancer le webservice, saisir dans un terminal : 
uvicorn main:app --reload


## Client de l'API 

Un second projet - disponible sur le dépôt git : 
'https://gitlab.com/Nohcip/client-calculatrice.git' - donne accès à la partie client de ce webservice. 

